import {Component, OnInit} from '@angular/core';
import {OfertasService} from '../ofertas.service';
import {Oferta} from '../shared/oferta.model';
import {ActivatedRoute, Params} from '@angular/router';
import 'rxjs/add/observable/interval';


@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.scss'],
  providers: [ OfertasService ]
})
export class OfertaComponent implements OnInit {

  public oferta: Oferta;

  constructor(private ofertasService: OfertasService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((parametros: Params) => {
      this.ofertasService.gerOfertaById(parametros.id)
        .then(
          (oferta: Oferta) => {
            this.oferta = oferta;
          }
        );
    });
  }
}
