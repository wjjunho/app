import { Oferta } from './shared/oferta.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { URL_API } from './app.api';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';

@Injectable()
export class OfertasService {

  constructor(private http: HttpClient) {
  }

  public getOfertasPorCategoria( categoria: string ): Promise<Oferta[]> {
    return this.http.get<Oferta[]>(`${(URL_API)}/ofertas?categoria=${categoria}` )
      .toPromise().then((resposta: Oferta[]) => resposta);
  }

  public getOfertas(): Promise<Oferta[]> {
    return this.http.get<Oferta[]>(`${(URL_API)}/ofertas` )
      .toPromise().then((resposta: Oferta[]) => resposta);
  }

  public gerOfertaById( id: number ): Promise<Oferta> {
    return this.http.get<Oferta>(`${(URL_API)}/ofertas?id=${id}`)
      .toPromise().then(
        (resposta: Oferta) => {
          return resposta[0];
        }
      );
  }

  public getComoUsarOfertaById( id: number ): Promise<string> {
    return this.http.get(`${(URL_API)}/como-usar?id=${id}`)
      .toPromise().then(
        (resposta: string) => {
          // @ts-ignore
          return resposta[0].descricao;
        }
      );
  }

  public getOndeFicaOfertaById( id: number ): Promise<string> {
    return this.http.get(`${(URL_API)}/onde-fica?id=${id}`)
      .toPromise().then(
        (resposta: any) => {
          return resposta[0].descricao;
        }
      );
  }

  public pesquisaOfertaByDescricao(descricao: string): Observable<Oferta[]> {
    return this.http.get(`${(URL_API)}/ofertas?descricao_oferta_like=${descricao}`)
      .retry(10)
      .map((resultado: any) => {
        return resultado;
      });
  }
}
