import { Component, OnInit } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Oferta } from '../shared/oferta.model';
import {Observable, Subject} from 'rxjs';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/distinct';

@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.scss'],
  providers: [ OfertasService ]
})
export class TopoComponent implements OnInit {

  igreja: Array<string> = ['IASD - Distrito Balneário',
                           'Igreja Balneário', 'Igreja Boa Vista',
                           'Igreja Perim', 'Igreja Paulo Pacheco',
                           'Igreja Brisas da Mata'];
  oferta: Oferta;
  ofertas: Observable<Oferta[]>;
  pesquisaSubject: Subject<string> = new Subject<string>();

  constructor(private ofertaService: OfertasService) { }

  ngOnInit(): void {
    this.ofertas = this.pesquisaSubject
      .debounceTime(1000)
      .distinct()
      .switchMap((termo: string) => {
        if (termo.trim() === '') {
          return Observable.of<Oferta[]>([]);
        }
        return this.ofertaService.pesquisaOfertaByDescricao(termo);
      })
      .catch((err: any) => {
        return Observable.of<Oferta[]>([]);
      });
  }

  pesquisar(termoDaPesquisa: string): void {
    this.pesquisaSubject.next(termoDaPesquisa);
  }

  limparPesquisa(): void {
    this.pesquisaSubject.next('');
  }
}
