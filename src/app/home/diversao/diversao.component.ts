import {Component, OnInit} from '@angular/core';
import { Oferta } from '../../shared/oferta.model';
import { OfertasService } from '../../ofertas.service';

@Component({
  selector: 'app-diversao',
  templateUrl: './diversao.component.html',
  styleUrls: ['./diversao.component.scss'],
  providers: [ OfertasService ]
})
export class DiversaoComponent implements OnInit {

  public ofertas: Array<Oferta>;

  constructor(private ofertaService: OfertasService) { }

  ngOnInit(): void {
    this.ofertaService.getOfertasPorCategoria('diversao')
      .then((ofertas: Array<Oferta>) => {
        this.ofertas = ofertas;
      }).catch(
      ( param: any) => console.log(param)
    );
  }

}
