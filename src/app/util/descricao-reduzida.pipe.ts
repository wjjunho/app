import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'descricaoReduzida'
})
export class DescricaoReduzida implements PipeTransform {
  transform(value: string, truncar: number): string {
    if (value.length > truncar) {
      return value.substr(0, truncar) + '...';
    }
    return value;
  }
}
