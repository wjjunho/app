import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { RestaurantesComponent } from './home/restaurante/restaurantes.component';
import { DiversaoComponent } from './home/diversao/diversao.component';
import {OfertaComponent} from './oferta/oferta.component';
import {ComoUsarComponent} from './oferta/como-usar/como-usar.component';
import {OndeFicaComponent} from './oferta/onde-fica/onde-fica.component';

export const ROUTES: Routes = [
  {path: '', component: HomeComponent},
  {path: 'inicio', component: HomeComponent},
  {path: 'oferta', component: OfertaComponent},
  {path: 'oferta', component: OfertaComponent},
  {path: 'oferta/:id', component: OfertaComponent,
      children: [
        { path: '', component: ComoUsarComponent },
        { path: 'como-usar', component: ComoUsarComponent },
        { path: 'onde-fica', component: OndeFicaComponent }
        ]
  },
  {path: 'diversao', component: DiversaoComponent},
  {path: 'restaurantes', component: RestaurantesComponent}
];
